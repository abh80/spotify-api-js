# Version

Get the version of spotify-api.js

```js
Spotify.version
```

**Minimum Version:** v3.x | **Recommended Version:** v4.x