# Installation

You can install spotify-api.js using npm

```bash
npm i spotify-api.js@latest
```

**Minimum Version:** v3.x | **Recommended Version:** v4.x