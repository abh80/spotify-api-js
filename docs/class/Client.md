# Client

Spotify Client

---
<h3 style="font-family: consolas;" id="constructor">constructor(<font style="opacity: 0.7; font-weight: light;">token?: string</font>)</h3>


---
<h3 style="font-family: consolas;" id="token">.token<font style="opacity: 0.7; font-weight: light;">: string</font></h3>

> Your auth token
> 

---
<h3 style="font-family: consolas;" id="lib">.lib<font style="opacity: 0.7; font-weight: light;">: { <a href=https://spotifyapijs.netlify.app/#/class/auth>Auth</a>, <a href=https://spotifyapijs.netlify.app/#/class/album>Album</a>, <a href=https://spotifyapijs.netlify.app/#/class/artist>Artist</a>, <a href=https://spotifyapijs.netlify.app/#/class/playlist>Playlist</a>, <a href=https://spotifyapijs.netlify.app/#/class/track>Track</a>, <a href=https://spotifyapijs.netlify.app/#/class/user>User</a> }</font></h3>

> Lib classes
> 

---
<h3 style="font-family: consolas;" id="utils">.utils<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/util>Util</a></font></h3>

> Util class
> 

---
<h3 style="font-family: consolas;" id="oauth">.oauth<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/auth>Auth</a></font></h3>

> Auth class
> 

---
<h3 style="font-family: consolas;" id="albums">.albums<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/album>Album</a></font></h3>

> Album class
> 

---
<h3 style="font-family: consolas;" id="artists">.artists<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/artist>Artist</a></font></h3>

> Artist class
> 

---
<h3 style="font-family: consolas;" id="playlists">.playlists<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/playlist>Playlist</a></font></h3>

> Playlist class
> 

---
<h3 style="font-family: consolas;" id="tracks">.tracks<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/track>Track</a></font></h3>

> Track class
> 

---
<h3 style="font-family: consolas;" id="users">.users<font style="opacity: 0.7; font-weight: light;">: <a href=https://spotifyapijs.netlify.app/#/class/user>User</a></font></h3>

> User class
> 