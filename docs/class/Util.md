# Util

Utility methods

---
<h3 style="font-family: consolas;" id="constructor">constructor(<font style="opacity: 0.7; font-weight: light;">token?: string</font>)</h3>


---
<h3 style="font-family: consolas;" id="token">.token<font style="opacity: 0.7; font-weight: light;">: string</font></h3>

> Your auth token
> 

---
<h3 style="font-family: consolas;" id="hextorgb">.hexToRgb(<font style="opacity: 0.7; font-weight: light;">hex: string</font>)</h3>

> Coverts hex to rgb
> 
> | PARAMETER   | TYPE    | DESCRIPTION    |
> |--------|---------|----------------|
> | hex | string | Hex code to convert |
> 
> **Returns:** number[] | void

---
<h3 style="font-family: consolas;" id="fetch">.fetch(<font style="opacity: 0.7; font-weight: light;">options: { link: string, params?: any, headers?: any }</font>)</h3>

> Easy to fetch spotify api
> 
> | PARAMETER   | TYPE    | DESCRIPTION    |
> |--------|---------|----------------|
> | options | { link: string, params?: any, headers?: any } | Fetch Options |
> 
> **Returns:** Promise<any>

---
<h3 style="font-family: consolas;" id="geturidata">.getURIData(<font style="opacity: 0.7; font-weight: light;">uri: string</font>)</h3>

> Get uri data
> 
> | PARAMETER   | TYPE    | DESCRIPTION    |
> |--------|---------|----------------|
> | uri | string | Uri |
> 
> **Returns:** Promise<any>

---
<h3 style="font-family: consolas;" id="getcodeimage">.getCodeImage(<font style="opacity: 0.7; font-weight: light;">uri: string</font>)</h3>

> Returns the code image
> 
> | PARAMETER   | TYPE    | DESCRIPTION    |
> |--------|---------|----------------|
> | uri | string | Uri |
> 
> **Returns:** Promise<any>