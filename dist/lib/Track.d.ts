/**
 * Track lib file
 */
import Spotify from "../Spotify";
/**
 * Class of all methods related to tracks
 */
declare class Track extends Spotify {
    /**
     * @param q Your query
     * @param options Options to configure your search...
     *
     * **Example:**
     * ```js
     * const track = await spotify.tracks.search("oh my god by alec benjamin", { limit: 1, }); // Searches for the track and limit will be 20 by default
       const advanced = await spotify.tracks.search("oh my god by alec benjamin", {
           limit: 1,
           advanced: true,
       }); // Same but this will return a `codeImage` and `dominantColor` key with it!
     * ```
     */
    search(q: string, options?: {
        limit?: null | string | number;
        advanced?: boolean;
    }): Promise<any>;
    /**
     * @param id Id of the track
     *
     * **Example:**
     * ```js
     * const track = await spotify.tracks.get("track id"); // Get tracks by id...
     * ```
     */
    get(id: string): Promise<any>;
    /**
     * @param id Id of the track
     *
     * **Example:**
     * ```js
     * const audioAnalysis = await spotify.tracks.audioAnalysis("track id"); // Get audio analysis of the track
     * ```
     */
    audioFeatures(id: string): Promise<any>;
    /**
     * @param id Id of the track
     *
     * **Example:**
     * ```js
     * const audioFeatures = await spotify.tracks.audioFeatures("track id"); // Get audio features of the track
     * ```
     */
    audioAnalysis(id: string): Promise<any>;
}
export default Track;
