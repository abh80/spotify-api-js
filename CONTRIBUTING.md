# Spotify-api.js

So before you attempt to contribute, you can get a clear idea where to make changes

- As the package is made in Typescript, you need to update in `src/` folder. Make sure to compile it in `dist/`. Or else mention in pull request.

- Do not make changes to `.npmignore`, `.gitignore` or `tsconfig.json`. If you do, make sure to mention why and what in Pull Request Description.

- Do not make unwanted utils so that package be light

- If you cannot make Typedoc to your new function, mention in Pull Request Description.

- If you are not sure, always make an issue before making the pull request

- Make your test files only in a `test/` folder

- Do not add unwanted files

- Do not make changes to `package.json` scripts. If you are doing kindly mention in pull request description!

- Do not update `docs/` unless you know what you are doing...

**Happy Contributing!**