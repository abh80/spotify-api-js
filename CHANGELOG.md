# Spotify-api.js Changelog

Version history of spotify-api.js since v4.0.5

---

## v4.1.0

- Cleaned the waste codes from v4.0.5
- Exports lib files directly
- No need of using 'NO TOKEN'. The class will use it when if you didnt provide a token!
- Default export will be a object containing all exports rather than Client!
- Readme.md has es6 imports example
- Added contributing.md so the people can know about repo b efore contributing

**Status:** Published | **Released:** 31st August 2020

---

## v4.0.5

- Made codes to 4 indents from 2 indents so that the code looks pretty
- More brief typedoc
- Fixed small bugs

**Status:** Published | **Released:** 30th August 2020
