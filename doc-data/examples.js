module.exports = {
    Client: {
        description: 'Example to connect with Spotify Client!',
        file: 'client'
    },
    Search : {
        description: 'Example to search for tracks and artists!',
        file: 'search'
    }
}
